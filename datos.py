from datetime import date

from actor import Actor
from pelicula import Pelicula
from contacto import Contacto
from doble import Doble
from base import Session, engine, Base

Base.metadata.create_all(engine)

session = Session()

#Peliculas
piratas_del_caribe = Pelicula("Piratas del Caribe", date(2014, 5, 2))
john_wick_3 = Pelicula("John Wick 3", date(2019, 7, 7))
iron_man = Pelicula("Iron Man 1", date(2008, 3, 12))

#Actores
jhonny_depp = Actor("Jhonny Depp", date(1968, 10, 8))
keanu_reeves = Actor("Keanu Reeves", date(1649, 11, 11))
robert_downey = Actor("Robert Downey Jr.", date(1969, 5, 5))

#Reparto
piratas_del_caribe.actor = [jhonny_depp]
john_wick_3.actor = [keanu_reeves]
iron_man.actor = [robert_downey]

#Contacto
contacto_jd = Contacto("123456", "Su Casa", jhonny_depp)
contacto_kr = Contacto("456789", "Su PH", keanu_reeves)
contacto_rd = Contacto("3000", "Su Mansion", robert_downey)

session.add(piratas_del_caribe)
session.add(john_wick_3)
session.add(iron_man)

session.add(contacto_jd)
session.add(contacto_kr)
session.add(contacto_rd)

session.commit()
session.close()
