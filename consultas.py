from datetime import date
from base import Session
from actor import Actor
from contacto import Contacto
from pelicula import Pelicula

session = Session()

peliculas = session.query(Pelicula).all()
if len(peliculas) == 0:
   print("No hay registro de peliculas")
else:
   print("Todas las peliculas:")
   for pelicula in peliculas:
      print(pelicula.titulo + " fue lanzado en " + str(pelicula.lanzamiento))

peliculas = session.query(Pelicula).filter(Pelicula.lanzamiento > date(2012, 1, 1)).all()
print("\nPeliculas Recientes")
if len(peliculas) == 0:
   print("No hay peliculas")
else:
   for pelicula in peliculas:
      print(pelicula.titulo + " fue lanzada posterior a 2012") 

peliculas_keanu = session.query(Pelicula).join(Actor, Reparto.actor).filter(Actor.nombre == 'Keanu Reeves').all()
if len(peliculas_keanu) == 0:
   print("No hay Keanu")
else:
   for pelicula in peliculas_keanu:
      print('\nKeanu ha participado en ' + pelicula.titulo)
