from datetime import date
from base import Session, engine, Base
from actor import Actor
from pelicula import Pelicula
from doble import Doble
from contacto import Contacto

Base.metadata.create_all(engine)

session = Session()
