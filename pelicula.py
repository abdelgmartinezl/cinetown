from sqlalchemy import Column,String,Integer,Date,Table,ForeignKey
from sqlalchemy.orm import relationship
from base import Base

reparto = Table(
   'reparto', Base.metadata,
    Column('id_pelicula', Integer, ForeignKey('pelicula.id')),
    Column('id_actor', Integer, ForeignKey('actor.id'))
)

class Pelicula(Base):
   __tablename__ = 'pelicula'

   id = Column(Integer, primary_key=True)
   titulo = Column(String(100))
   lanzamiento = Column(Date)
   actor = relationship("Actor", secondary=reparto)

   def __init__(self, titulo, lanzamiento):
      self.titulo = titulo
      self.lanzamiento = lanzamiento
